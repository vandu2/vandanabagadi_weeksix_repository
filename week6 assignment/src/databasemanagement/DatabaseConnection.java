package databasemanagement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
private static DatabaseConnection instance;
private static Connection connection;
private DatabaseConnection() {

	}
	public static DatabaseConnection getInstance() {
		//Singleton design pattern for database connection
		if(instance == null) {
			instance = new DatabaseConnection();
		}
		return instance;
	}
	public Connection getConnection() {	
		//if database is not connected, it creates a new connection else returns existing one
		if(connection == null) {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				connection = DriverManager.getConnection(
						"jdbc:mysql://localhost:3306/vandana_bagadi","root","Siri@999");
				System.out.println("driver loaded and connected");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				System.out.println("error driver not loaded");
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println("cannot connect to database");
				e.printStackTrace();
			}
		}
		return connection;	
	}

}

