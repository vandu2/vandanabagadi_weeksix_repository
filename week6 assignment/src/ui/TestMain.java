package ui;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import model.Movie;
import moviesproject.Classification;
import moviesproject.MoviesFactory;
public class TestMain {
	public static void main(String[] args) {
		int choice;
        Scanner sc = new Scanner(System.in);
		do
		{
            System.out.println("WELCOME");
			System.out.println("Please select a MoviesList");
			System.out.println(
					"1)Movies comming movies\n"
							+ "2)Movies In Theaters\n"
							+ "3)Top Rated Indian\n"
							+ "4)Top Rated Movies\n"
							+ "5) to exit");
			System.out.println("Enter your choice");
			System.out.println();

			choice = sc.nextInt();

			switch(choice) {
			case 1:
				Classification cs=MoviesFactory.setMovieType(1);
				try {
					List<Movie> movie=cs.movieType();
					System.out.println("======Coming Soon Movies======");
					for(Movie mo:movie) {
						System.out.println(mo+ "\n");
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
				break;
			case 2:
				Classification mit=MoviesFactory.setMovieType(2);
				try {
					List<Movie> movie=mit.movieType();
					System.out.println("======Movies in Theaters======");
					for(Movie mo:movie) {
						System.out.println(mo+ "\n");
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
				break;
			case 3:
				Classification tri=MoviesFactory.setMovieType(3);
				try {
					List<Movie> movie=tri.movieType();
					System.out.println("======Top Rated Indian======");
					for(Movie mo:movie) {
						System.out.println(mo+ "\n");
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
				break;
			case 4:
				Classification trm=MoviesFactory.setMovieType(4);
				try {
					List<Movie> movie=trm.movieType();
					System.out.println("======Top Rated Movies======");
					for(Movie mo:movie) {
						System.out.println(mo+ "\n");
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
				break;

			case 5:
				System.exit(0);
				break;
			default:
				System.out.println("Invalid Choice");
				break;
			}
		}while(choice!=5);
	}

}


